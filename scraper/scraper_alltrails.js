// Import the playwright library into our scraper.
const playwright = require('playwright');

async function main() {
    // Open a Chromium browser. We use headless: false
    // to be able to watch what's going on.
    const browser = await playwright.chromium.launch({
        headless: false
    });
    // Open a new page / tab in the browser.
    const page = await browser.newPage();
    // Tell the tab to navigate to the JavaScript topic page.
    // await page.goto('https://www.alltrails.com/trail/hong-kong/tai-po/ngam-tau-shan-and-wa-mei-shan');
    await page.goto('https://www.alltrails.com/trail/hong-kong/sai-kung/maclehose-trail-section-3-west-to-east');
    // Extract the required information from the site

    setTimeout(async () => {
        const result = await page.evaluate(() => {
            let name = ""
            let getNames = document.querySelectorAll('.styles-module__name___1nEtW')
            for (getName of getNames) {
                if (getName.getAttribute('itemprop') === "name") {
                    name = getName.innerText
                }
            }
            let region = ""
            let listItems = document.querySelectorAll('li')
            for (let i=0; i<listItems.length;i++){
                if(i==1){
                    getRegions=listItems[i].querySelectorAll('.xlate-none')
                    for (getRegion of getRegions){
                        if (getRegion.getAttribute('itemprop')==="name"){
                            region=getRegion.innerText
                        }
                    }
                }
            }
            let district = ""
            let getDistricts = document.querySelectorAll('.styles-module__location___3wEnO')
            for (getDistrict of getDistricts) {
                if (getDistrict.getAttribute('title')) {
                    district = getDistrict.innerText
                }
            }
            let length = ""
            let getLengths = document.querySelectorAll('.styles-module__statsLabel___1zGTI')
            for (getLength of getLengths) {
                if (getLength.innerText === "Length") {
                    length = getLength.nextElementSibling.innerText
                }
            }
            let elevation = ""
            let getElevations = document.querySelectorAll('.styles-module__statsLabel___1zGTI')
            for (getElevation of getElevations) {
                if (getElevation.innerText === "Elevation gain") {
                    elevation = getElevation.nextElementSibling.innerText
                }
            }
            let type = ""
            let getTypes = document.querySelectorAll('.styles-module__statsLabel___1zGTI')
            for (getType of getTypes) {
                if (getType.innerText === "Route type") {
                    type = getType.nextElementSibling.innerText
                }
            }
            let time= ""
            container=document.querySelector('#content').firstElementChild.getAttribute('data-react-props')
            let index=container.indexOf('duration_minutes')
            substring=container.substring(index,index+30)
            commaIndex=substring.indexOf(',')
            time=container.substring(index+18,index+commaIndex)+" mins"
            
            const description = document.querySelector('#auto-overview').innerText;
            let note = document.querySelector('#text-container-description').innerText;
            let difficulty = 0
            let getStars = document.querySelector('#title-and-menu-box').querySelectorAll('img')
            for (getStar of getStars) {
                if (getStar.alt === "Yellow Star") {
                    if (getStar.parentElement.classList.contains('MuiRating-iconFilled')) {
                        difficulty+=0.5
                    }
                }
            }
            return {
                name: name,
                type: type,
                region: region,
                district: district,
                length: length,
                time: time,
                elevation_gain: elevation,
                description: description,
                note: note,
                difficulty: difficulty
            };
        });
        console.log(result);
        // Pause for 10 seconds, to see what's going on.
        //await page.waitForTimeout(10000);
        // Turn off the browser to clean up after ourselves.
        await browser.close();
    }, 0000)
}

main();