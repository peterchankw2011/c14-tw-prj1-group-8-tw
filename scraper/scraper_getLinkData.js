// Import the playwright and other required modules.
const playwright = require('playwright');
const fs = require('fs');

async function getData(path, district) {
    // Open a Chromium browser. We use headless: false
    // to be able to watch what's going on.
    const browser = await playwright.chromium.launch({
        headless: true
    });
    // Open a new page / tab in the browser.
    const page = await browser.newPage();
    await page.setDefaultNavigationTimeout(0);
    // Tell the tab to navigate to the JavaScript topic page.
    await page.goto(path);
    //await page.goto('https://follo3me.com/nent/fan-kei-tok/');

    // Extract the required information from the site
    const result = await page.evaluate((region) => {

        const name = document.querySelector('h1').innerText;
        let description;
        //description scraping
        if (document.querySelector('img[alt="簡介"]').parentElement.nextElementSibling) {
            description = document.querySelector('img[alt="簡介"]').parentElement.nextElementSibling.innerText;
        } else if (document.querySelector('img[alt="簡介"]').nextElementSibling) {
            description = document.querySelector('img[alt="簡介"]').nextElementSibling.innerText;
        } else if (document.querySelector('img[alt="簡介"]').parentElement.parentElement.nextElementSibling) {
            description = document.querySelector('img[alt="簡介"]').parentElement.parentElement.nextElementSibling.innerText;
        } else if (document.querySelector('.auto-style51')) {
            description = document.querySelector('.auto-style51').innerText;
        } else {
            description = "";
        }

        let view = 0;
        //count the number of stars representing view
        const stars = document.querySelectorAll('img');
        for (image of stars) {
            if (image.getAttribute('src') == "https://i0.wp.com/follo3me.com/wp-content/uploads/2016/04/star_full.png?resize=26%2C26&ssl=1") {
                view += 1;
            }
        };

        const others = [];

        const tables = document.querySelectorAll('tr');
        //loop over the info of tables, ignore the first two records
        for (let i = 2; i < tables.length; i++) {
            others.push(tables[i].innerText.trim().replaceAll("\n", ""))
        }


        const canvasContents = document.querySelector('.wpgpxmaps_summary').innerText;

        return {
            name: name,
            district: region,
            description: description,
            view: view,
            dimension: canvasContents,
            others: others,
        }
    }, district);
    // Pause for 10 seconds, to see what's going on.
    //await page.waitForTimeout(10000);
    // Turn off the browser to clean up after ourselves.
    await browser.close();

    //initialize for data cleaning
    let new_dimension = {};
    let new_others = {};
    result['route'] = {};
    result['transport'] = {};
    let starting_index = result['others'].length;
    let transport_index = result['others'].length;
    let transport_header = [];
    let transport_count = 0

    for (let i = 0; i < result['others'].length; i++) {
        let split_item = result['others'][i].split('\t');
        //finding the index of 起點&交通
        if (split_item[0] == '起點') {
            starting_index = i;

        } else if (split_item[0] == '交通') {
            transport_index = i;
        }


        if (split_item[1]) {
            if (i == starting_index) {
                transport_header = split_item;
            } else if (i == starting_index + 1) {
                for (let i = 0; i < split_item.length; i++) {
                    result['route'][transport_header[i]] = split_item[i];
                }
            } else if (i < transport_index) {
                new_others[split_item[0]] = split_item[1];
            } else if (i > transport_index) {
                const transport_info = {};
                transport_info['交通'] = split_item[0];
                transport_info['路線'] = split_item[1];
                transport_info['上/下車點'] = split_item[2];
                if (split_item[3]) {
                    transport_info['備註'] = split_item[3];
                } else {
                    transport_info['備註'] = "";
                }
                result['transport'][transport_count.toString()] = transport_info
                transport_count += 1;
                // result['transport'].push(transport_info);
            } else {

            }
        }
    };

    //data cleaning for dimension
    const split_dimension = result['dimension'].split('\n');
    split_dimension.pop();
    for (item of split_dimension) {
        new_dimension[item.split(":")[0].replace(" ", "_")] = parseFloat(item.split(":")[1])
    }


    //data cleaning for time
    let hours = parseInt(new_others['需時'].split('小時')[0]);
    let mins = parseInt(new_others['需時'].split('小時')[1]);
    result['time'] = hours * 60 + mins;

    result['length'] = parseFloat(new_others['路程']);
    result['difficulty'] = parseFloat(new_others['難度']);

    delete result.dimension;
    delete result.others;
    delete new_others['需時'];
    delete new_others['路程'];
    delete new_others['難度'];

    result['dimension'] = new_dimension;
    result['others'] = new_others;

    //export data to JSON
    let data = JSON.stringify(result);

    await fs.writeFile(`./data/route_info/${result.name}.json`, data, (err) => {
        if (err) throw err;
        console.log(`${result.name}: written`)
    });

    return result
};

//checking function
async function checking() {
    console.log(await getData("https://follo3me.com/islands/sharp-island/", "離島.其他島嶼"))
}

exports.getData = getData;
