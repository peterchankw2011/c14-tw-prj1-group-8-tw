// Import the playwright library into our scraper.
const playwright = require('playwright');

async function main() {
    // Open a Chromium browser. We use headless: false
    // to be able to watch what's going on.
    const browser = await playwright.chromium.launch({
        headless: false
    });
    // Open a new page / tab in the browser.
    const page = await browser.newPage();
    // Tell the tab to navigate to the JavaScript topic page.
    await page.goto('https://www.oasistrek.com/castle_peak.php');
    // Extract the required information from the site
    const result = await page.evaluate(() => {
        const name = document.querySelector('.name').innerText;
        const route = document.querySelector('.route').innerText;
        const transport = document.querySelector('.transport').innerText;
        const distance = document.querySelector('.distance').innerText;
        const infos = document.querySelectorAll('.info');
        let arr = [];
        for (info of infos) {
            if (info.innerText == "") {
            } else {
                arr.push(info.innerText);
            }
        }
        //count the number of stars representing difficulty
        const stars = document.querySelectorAll('.info >.star > img');
        let difficulty = 0;
        let view = 0;
        for (image of stars) {
                if (image.getAttribute('src') == "images/content_images/star_icon.gif") {
                    difficulty += 1;
                }else if(image.getAttribute('src') == "images/content_images/star2_icon.gif"){
                    view += 1;
                }else{

                }
        }
        return {
            name: name,
            route: route,
            transport: transport,
            distance: distance,
            difficulty: difficulty,
            view: view,
            info: arr
        };
    });
    console.log(result);
    // Pause for 10 seconds, to see what's going on.
    //await page.waitForTimeout(10000);
    // Turn off the browser to clean up after ourselves.
    await browser.close();
}

main();