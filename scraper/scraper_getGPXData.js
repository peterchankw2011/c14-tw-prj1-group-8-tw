// Import the playwright and other required modules.
const playwright = require('playwright');
const fs = require('fs');


async function getGPX_link(path) {
    // Open a Chromium browser. We use headless: false
    // to be able to watch what's going on.
    const browser = await playwright.chromium.launch({
        headless: true,
    });

    // Open a new page / tab in the browser.
    const page = await browser.newPage();
    await page.setDefaultNavigationTimeout(0);
    // Tell the tab to navigate to the JavaScript topic page.
    await page.goto(path);

    // Extract the required information from the site
    const result = await page.evaluate((url) => {
        const name = document.querySelector('h1').innerText;
        const all_links = document.querySelectorAll('a');
        let gpx_link = "";
        for (link of all_links){
            if(link.innerText == "下載路線"){
                gpx_link = link.getAttribute('href')
            }
        }

        return {
            name: name,
            page_link: url,
            gpx_link: gpx_link

        }
    }, path)

    await browser.close();


    //export data to JSON
    let data = JSON.stringify(result);

    await fs.writeFile(`./data/route_GPX/GPX_links.json`, data, { flag: 'a' }, (err) => {
        if (err) throw err;
        console.log(`GPS links captured: ${result.name}`)
    });

    return result
};

//checking function
async function checking() {
    console.log(await getGPX_link('https://follo3me.com/hk/yuk-kwai-shan/'))
}

exports.getGPX_link = getGPX_link;
