// Import the playwright and other required modules.
const playwright = require('playwright');
const fs = require('fs');


async function getPic_link(path) {
    // Open a Chromium browser. We use headless: false
    // to be able to watch what's going on.
    const browser = await playwright.chromium.launch({
        headless: true,
    });

    // Open a new page / tab in the browser.
    const page = await browser.newPage();
    await page.setDefaultNavigationTimeout(0);
    // Tell the tab to navigate to the JavaScript topic page.
    await page.goto(path);

    // Extract the required information from the site
    const result = await page.evaluate((url) => {
        const name = document.querySelector('h1').innerText;
        const pic_link = document.querySelector(".entry-thumbnail > img").getAttribute('src');

        return {
            name: name,
            page_link: url,
            pic_link: pic_link

        }
    }, path)

    await browser.close();


    //export data to JSON
    let data = JSON.stringify(result);

    await fs.writeFile(`./data/route_pic/pic_links.json`, data, { flag: 'a' }, (err) => {
        if (err) throw err;
        console.log(`Pic links captured: ${result.name}`)
    });

    return result
};

//checking function
async function checking() {
    console.log(await getPic_link('https://follo3me.com/hk/yuk-kwai-shan/'))
}

exports.getPic_link = getPic_link;