// Import the playwright library into our scraper.
const playwright = require('playwright');
const fs = require('fs');



async function get_links(path, region) {
    let links = [];
    // Open a Chromium browser. We use headless: false
    // to be able to watch what's going on.
    const browser = await playwright.chromium.launch({
        headless: true
    });
    // Open a new page / tab in the browser.
    const page = await browser.newPage();
    await page.setDefaultNavigationTimeout(0); 
    // Tell the tab to navigate to the JavaScript topic page.
    await page.goto(path);

    // Extract the required information from the site
    const result = await page.evaluate((location) => {
        let link_list = [];
        const links = document.querySelectorAll(`a[href^='${CSS.escape(location)}']`);
        for (link of links) {
            link_list.push(link.getAttribute('href'));
        }
        return link_list.slice(2, link_list.length-7)

    },path);
    // Pause for 10 seconds, to see what's going on.
    //await page.waitForTimeout(10000);

    // Turn off the browser to clean up after ourselves.

    await browser.close();

    //Data cleaning
    links[region] = result;

    //export data to JSON
    let data = JSON.stringify(links);

    await fs.writeFile(`./data/all_route_link.json`, data,{flag:'a'},(err) => {
        if (err) throw err;
        console.log(`Data written: ${region}`)
    });
    return links
}

//checking function
async function checking() {
    console.log(await get_links('https://follo3me.com/hk','香港島'))
}

exports.get_links = get_links;

//checking();