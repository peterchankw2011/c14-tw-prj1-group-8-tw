const { chromium } = require('playwright');

(async () => {
  const browser = await chromium.launch({
    headless: false
  });
  const context = await browser.newContext();

  // Open new page
  const page = await context.newPage();

  // Go to https://www.alltrails.com/trail/hong-kong/sai-kung/maclehose-trail-section-3-west-to-east
  await page.goto('https://www.alltrails.com/trail/hong-kong/sai-kung/maclehose-trail-section-3-west-to-east');

  // Click [data-test-id="navigation-loginButton"] [data-test-id="mainButton"] div:has-text("Log In")
  await page.click('[data-test-id="navigation-loginButton"] [data-test-id="mainButton"] div:has-text("Log In")');
  // assert.equal(page.url(), 'https://www.alltrails.com/login?ref=header');

  // Click input[name="userEmail"]
  await page.click('input[name="userEmail"]');

  // Fill input[name="userEmail"]
  await page.fill('input[name="userEmail"]', 'tecky_group8_hiking@gmail.com');

  // Press Tab
  await page.press('input[name="userEmail"]', 'Tab');

  // Fill input[name="userPassword"]
  await page.fill('input[name="userPassword"]', 'peteredwardjustin');

  // Click [data-test-id="formButton-submit"]
  await Promise.all([
    page.waitForNavigation(/*{ url: 'https://www.alltrails.com/trail/hong-kong/sai-kung/maclehose-trail-section-3-west-to-east' }*/),
    page.click('[data-test-id="formButton-submit"]')
  ]);

  // Click text=MoreSuggest EditDownload RouteDownload RouteSelect a file formatGPX TrackGoogle  >> a
  await page.click('text=MoreSuggest EditDownload RouteDownload RouteSelect a file formatGPX TrackGoogle  >> a');

  // Click #title-and-menu-box a:has-text("More")
  await page.click('#title-and-menu-box a:has-text("More")');

  // Click text=Download Route
  await page.click('text=Download Route');

  // Select csv
  await page.selectOption('select[name="filetype"]', 'csv');

  // Click button:has-text("Download")
  const [download] = await Promise.all([
    page.waitForEvent('download'),
    page.click('button:has-text("Download")')
  ]);

  // Close page
  await page.close();

  // ---------------------
  await context.close();
  await browser.close();
})();