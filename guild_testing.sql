/* Users and guild related tables */
select * from users;

Select *
from guilds;

select * from guild_user;

select * from guild_user_request;

DELETE FROM guilds WHERE id = 2;

UPDATE users SET guild_id = NULL WHERE id = 2;


UPDATE guilds
          SET experience = experience + 400
          WHERE id = 1


INSERT INTO guild_user_request (
        guild_id,
        user_id,
        request_at
    )
values (0, 0, NOW());

SELECT id FROM guild_user_request WHERE guild_id = 10 AND user_id = 4 ;

SELECT guild_user_request.guild_id, guild_user_request.user_id, users.nickname
FROM guild_user_request, users
WHERE guild_user_request.user_id = users.id AND guild_user_request.guild_id = 11 

SELECT * FROM guild_user_request WHERE guid_user_request

SELECT guilds.id, guilds.name, users.nickname, COUNT(guild_user.id) AS member
FROM guilds, guild_user, users
WHERE guilds.id = guild_user.guild_id, guild_user.user_id = users.id
GROUP BY guilds.id

DELETE FROM guild_user;
DELETE FROM guilds;



    
SELECT  guild_user.guild_id, users.nickname
FROM users, guild_user
WHERE users.id = guild_user.user_id AND guild_user.position = '會長'
LEFT JOIN (SELECT guilds.id, guilds.name, COUNT(guild_user.id) As member_num
    FROM guilds, guild_user
    WHERE guilds.id = guild_user.guild_id
    GROUP BY guilds.id) AS guild_list on guild_list.id = guild_user.user_id






SELECT threads.id, routes.district, routes.name, threads.title, threads.upvote, threads.created_at, sorted_comments.max_updated_at
    FROM threads
    LEFT JOIN routes on threads.route_id = routes.id
    LEFT JOIN
    (SELECT comments.thread_id AS thread_id ,max(comments.updated_at) AS max_updated_at
    FROM comments 
    GROUP BY comments.thread_id) AS sorted_comments on threads.id = sorted_comments.thread_id
    ORDER BY max_updated_at DESC 

INSERT INTO guilds (
        name,
        level,
        experience,
        created_at,
        updated_at
    )
values ('testing2', 0, 0, NOW(), NOW());


/* Activities related tables */

SELECT * FROM activities;
SELECT * FROM participants_activities;
SELECT * FROM routes

SELECT * FROM requests_activity;


/* my team activities */
SELECT activities.id, routes.name, activities.guild_id, activities.status, activities.date
FROM activities, routes
WHERE activities.route_id = routes.id AND activities.leader_id = 1

/* joined activities */

SELECT activities.id, routes.name, activities.guild_id, activities.status, activities.date
FROM participants_activities, activities, routes
WHERE activities.route_id = routes.id AND participants_activities.activity_id = activities.id AND participants_activities.user_id = 1

/* my finished team activity by ID */
SELECT activities.id, routes.difficulty, activities.guild_id, activities.status, activities.leader_id, activities.date
FROM activities, routes
WHERE activities.route_id = routes.id AND activities.id = 1;

/* my finished activity participant by ID */

SELECT activities.id,  activities.guild_id, activities.status, participants_activities.user_id, activities.date
FROM participants_activities, activities 
WHERE participants_activities.activity_id = activities.id AND participants_activities.activity_id = 1

/* update user exp */

SELECT * FROM users

UPDATE users
SET experience = 0
WHERE id = 1 OR id = 2 OR id = 7



/* danger! dont use!!!!! */
DROP TABLE users;
DROP TABLE guilds;
DROP TABLE guild_user;
DROP TABLE guild_user_request;
DROP TABLE activities;
DROP TABLE participants_activities;
DROP TABLE requests_activity;
DROP TABLE threads;
DROP TABLE comments;
/* danger! dont use!!!!! */
