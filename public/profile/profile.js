document.querySelector(".return").addEventListener("click", async () => {
  const res = await fetch("/index");
  if (res.status == 200) {
    window.history.back();
  }
});

document.querySelector("#button_home").addEventListener("click", async () => {
  const res = await fetch("/index");
  if (res.status == 200) {
    window.location = "/index/index.html";
  }
});

async function loadUserProfile() {
  //fetch and update user infos
  const res = await fetch("/index");
  const result = await res.json();

  const haveImg = function () {
    if (result.profilepic) {
      return `<img src="../uploads/${result.profilepic}" class="img-fluid propic" alt="profile picture">`;
    } else {
      return `<img src="../uploads/inv_misc_questionmark.jpg" class="img-fluid propic" alt="profile picture">`;
    }
  };

  document.querySelector(".player-profile").innerHTML = `
  <div class="propic-container">
      ${haveImg()}
  </div>
  <div class="profile-container">
          <div class="profile-content">玩家編號: ${result.id}</div>
          <div class="profile-content">名稱: ${result.nickname}</div>
          <div class="profile-content">公會: ${result.guildName} </div>
  </div>`

  document.querySelector(".exp-bar").innerHTML = `
  <div>等級 ${result.level}</div>
    <div class="meter">
    <div style="width: ${result.current_exp / 10}%"></div>
    <span id="current-exp"> ${result.current_exp}/1000</span>
  `


  //fetch and update activities journal
  const res_journal = await fetch("/journal")
  const journal = await res_journal.json();

  const journals_container = document.querySelector("#journals")
  journals_container.innerHTML = ""

  if (journal.host[0] || journal.joined[0]) {
    if (journal.host[0]) {
      for (activity of journal.host) {
        journals_container.innerHTML +=
          `            <tr id="host">
          <td>
              <div routeID=${activity.route_id} class="route-icon district-icon b${parseInt(activity.difficulty)}"
                  style="background-image: url('../route_pic/${activity.name}.jpg')"></div>
              <div class="route-name">${activity.name.split(" ")[0]}</div>
          </td>
          <td class="activity-content">
              <span>${activity.date.split(" ")[0]}</span>
              <span>${activity.date.split(" ")[1]}</span>
          </td>
          <td class="activity-content">My team</td>
          <td class="activity-content">${activity.status}</td>
          <td class="activity-content">${activity.difficulty*100}</td>
          <td><button activityID=${activity.id} type="button"
                  class="btn btn-secondary btn-sm btn-block">View</button></td>
      </tr>`
      }
    }
    if (journal.joined[0]) {
      for (activity of journal.joined) {
        journals_container.innerHTML +=
          `            <tr id="joined">
        <td>
            <div routeID=${activity.route_id} class="route-icon district-icon b${parseInt(activity.difficulty)}"
                style="background-image: url('../route_pic/${activity.name}.jpg')"></div>
            <div class="route-name">${activity.name.split(" ")[0]}</div>
        </td>
        <td class="activity-content">
            <span>${activity.date.split(" ")[0]}</span>
            <span>${activity.date.split(" ")[1]}</span>
        </td>
        <td class="activity-content">${activity.nickname}'s team</td>
        <td class="activity-content">${activity.status}</td>
        <td class="activity-content">${activity.difficulty*100}</td>
        <td><button activityID=${activity.id} type="button"
                class="btn btn-secondary btn-sm btn-block">View</button></td>
    </tr>`
      }
    }

    let routeIcons = document.querySelectorAll('.route-icon')
    for (let routeIcon of routeIcons) {
      routeIcon.addEventListener('click', function (event) {
        // console.log(event.currentTarget.getAttribute('routeid'))
        window.location = (`/route/route.html?routeID=${event.currentTarget.getAttribute('routeid')}`)
      })
    }
    let activityIcons = document.querySelectorAll('.btn')
    for (let activityIcon of activityIcons) {
      activityIcon.addEventListener('click', function (event) {
        window.location = (`/activityinfo/activityinfo.html?activityID=${event.currentTarget.getAttribute('activityID')}`)
      })
    }
  } else {
    journals_container.innerHTML = `<div id="no_activity">請加入或創建隊伍! </div>`
  }
}

loadUserProfile();
