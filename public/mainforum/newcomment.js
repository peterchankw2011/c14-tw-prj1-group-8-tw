const id = document.location.href.split("?")[1].split("=")[1];

async function getFirstThread() {
  const res = await fetch(`/getThread/${id}`);
  const result = await res.json();
  const header = document.querySelector("header");
  if (res.status === 200) {
    data = result;
    console.log(data.rows[0]);
    header.innerHTML = `<header>
    
    <div class="control">
    <div class="return">
    <i class="fas fa-chevron-left"></i>
  </div>
      <h2>${data.rows[0].title}</h2>
      
    </div>
  </header>`;
  }
  document.querySelector(".return").addEventListener("click", () => {
    window.location = `/mainforum/forumthread.html?id=${id}`;
  });
}

getFirstThread();

document
  .querySelector("#textarea-form")
  .addEventListener("submit", async (event) => {
    event.preventDefault();

    const form = event.target;
    const formData = new FormData();

    formData.append("content", form.content.value);
    if (form.image.files[0]) {
      formData.append("image", form.image.files[0]);
    }

    const res = await fetch(`/comment/${id}`, {
      method: "POST",
      body: formData,
    });

    const result = await res.json();
    console.log(result);

    form.reset();
    window.location = `/mainforum/forumthread.html?id=${id}`;
  });
