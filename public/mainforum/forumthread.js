const id = document.location.href.split("?")[1].split("=")[1];

const main = document.querySelector("main");
const header = document.querySelector("header");

const haveProPic = function (propic) {
  if (propic) {
    return `../uploads/${propic}`;
  } else {
    return '../uploads/inv_misc_questionmark.jpg';
  }
};

async function getFirstThread() {
  const res = await fetch(`/getThread/${id}`);
  const result = await res.json();
  if (res.status === 200) {
    data = result;
    console.log(data.rows[0]);
    const haveImg = function () {
      if (data.rows[0].image) {
        return `<img class="comment-img img-fluid mt-1" alt="its an photo" src="../uploads/${data.rows[0].image}" />`;
      } else {
        return "";
      }
    };
    header.innerHTML = `<header>
    
    <div class="control">
    <div class="return">
    <i class="fas fa-chevron-left"></i>
  </div>
      <h2>${data.rows[0].title}</h2>
      <i class="fas fa-plus"></i>
    </div>
  </header>`;
    main.innerHTML = ` <div class="card card-body first">
    <div class="top-container">
      <div class="thread-header">
        <div class="comment-num">#1</div>
        <div class="user-info">
          <div class="chat-pic" style="background-image: url(${haveProPic(data.rows[0].profilepic)})"></div>
          <div class="username">${data.rows[0].nickname}</div>
        </div>
        <div class="time">${data.rows[0].created_at}</div>
      </div>
      <div class="thread-body">
        <p>
         ${data.rows[0].content}
         ${haveImg()}
        </p>
      </div>
    </div>
    <div class="thread-footer">
      <div>
      <i class="fas fa-thumbs-up"></i>
        <p>${data.rows[0].upvote}</p>
      </div>
      
    </div>
  </div>`;
  }
  document.querySelector(".return").addEventListener("click", () => {
    window.location = "/mainforum/forumcat.html";
    // window.history.back();
  });

  document.querySelector(".fa-plus").addEventListener("click", () => {
    window.location = `/mainforum/newcomment.html?id=${id}`;
  });
}

//由comment table 到拎返 comment既 user_id content  upvote created_at
async function loadAllComments() {
  const res = await fetch(`/allComment/${id}`);
  const result = await res.json();

  if (res.status === 200) {
    data = result;
    //loop all the entities
    for (let i = 0; i < data.rows.length; i++) {
      const haveImg = function () {
        if (data.rows[i].image) {
          return `<img class="comment-img img-fluid mt-1" alt="its an photo" src="../uploads/${data.rows[i].image}" />`;
        } else {
          return "";
        }
      };
      const cardClass = document.createElement("div");
      cardClass.classList.add("card");
      cardClass.classList.add("card-body");
      cardClass.innerHTML += `<div class="top-container">
      <div class="thread-header">
        <div class="comment-num">#${i + 2}</div>
        <div class="user-info">
          <div class="chat-pic" style="background-image: url(${haveProPic(data.rows[i].profilepic)})"></div>
          <div class="username">${data.rows[i].nickname}</div>
        </div>
        <div class="time">${data.rows[i].created_at}</div>
      </div>
      <div class="thread-body">
        <p>
         ${data.rows[i].content}
         ${haveImg()}
        </p>
      </div>
    </div>
    <div class="thread-footer">
      <div>
      <i class="fas fa-thumbs-up"></i>
        <p>${data.rows[i].upvote}</p>
      </div>
      
    </div>`;
      main.appendChild(cardClass);
    }
  }
}

getFirstThread();
loadAllComments();
