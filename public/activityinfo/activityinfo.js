// let map;
//get id
socket.on("id", (id) => {
    //   console.log(id);
    ownId = id;
});

socket.on("request", (msg) => {
    console.log(msg + "a@@@@")
    loadActivityRequest()
})

var activityID = (document.location.href).split('?')[1].split('=')[1]

document.querySelector(".back").addEventListener('click', async function () {
    const res = await fetch("/index");
    if (res.status == 200) {
        window.history.back();
    }
})

document.querySelector(".today_weather").addEventListener('click', function () {
    document.querySelector(".today_weather_details").classList.toggle('display_none')
})
async function weatherForcast() {
    var res = await fetch('https://data.weather.gov.hk/weatherAPI/opendata/weather.php?dataType=fnd&lang=en')
    var forecast = await res.json();
    // const notification = document.querySelector('#head_info');
    // console.log(JSON.stringify(forcast))
    return forecast
}

async function headerLogWeather() {
    let weatherForecast = await weatherForcast()
    let generalSituation = weatherForecast['generalSituation']
    let futureForecasts = weatherForecast['weatherForecast']
    let today = new Date()
    let todayNums = (today.toLocaleDateString()).split('/')
    let day = (parseInt(todayNums[0][0]) == 0 ? todayNums[0][1] : todayNums[0])
    let month = (parseInt(todayNums[1][0]) == 0 ? todayNums[1][1] : todayNums[1])
    document.querySelector('.today_weather').innerText = `${day + "/" + month}`
    document.querySelector('.today_weather_details').innerText = `${generalSituation}`
    // console.log(JSON.stringify(generalSituation) + "generalSituation")
    let forecastInnerHTML = ''
    for (let forecast of futureForecasts) {
        let forecastDay = forecast['forecastDate'].substring(4, 6)
        let forecastMonth = forecast['forecastDate'].substring(6, 8)
        forecastsDay = ((forecastDay[0] == 0) ? forecastDay[1] : forecastDay)
        forecastMonth = ((forecastMonth[0] == 0) ? forecastMonth[1] : forecastMonth)
        let forecastWeek = forecast['week']
        let iconID = forecast['ForecastIcon']
        // console.log(forecastDay + '/' + forecastMonth)
        forecastInnerHTML += `<div class="day">
                                <div class="weather_day">
                                  <div class="date">${forecastDay}/${forecastMonth}<br>${forecastWeek}</div>
                                  <img src="../weather_icons/pic${iconID}.png" class="weather_image"  height="70%">
                                </div>
                              </div>`
        document.querySelector('.weather_forecast').innerHTML = forecastInnerHTML
        // console.log(JSON.stringify(forecast) + "forecast")
    }
}
headerLogWeather()

loadActivityInfo()


/////////////////google map///////////
// function initMap() {
//     const directionsService = new google.maps.DirectionsService();
//     const directionsRenderer = new google.maps.DirectionsRenderer();
//     map = new google.maps.Map(document.getElementById("map"), {
//         center: { lat: 22.408003, lng: 114.125350 },
//         zoom: 13,
//     });
//     // map.setCenter({ lat: 22.302711, lng: 114.177216 })
//     map.setMapTypeId('terrain')
//     directionsRenderer.setMap(map);
//     calculateAndDisplayRoute(directionsService, directionsRenderer);
// }

// function calculateAndDisplayRoute(directionsService, directionsRenderer) {
//     const waypts = [{ location: { lat: 22.408003, lng: 114.125350 }, stopover: true }];
//     directionsService.route(
//         {
//             origin: ({ lat: 22.440662, lng: 114.126737 }),
//             destination: ({ lat: 22.395255, lng: 114.108444 }),
//             waypoints: waypts,
//             //optimizeWaypoints: true,
//             travelMode: google.maps.TravelMode.WALKING,
//         },
//         (response, status) => {
//             if (status === "OK" && response) {
//                 directionsRenderer.setDirections(response);
//             } else {
//                 window.alert("Directions request failed due to " + status);
//             }
//         }
//     );
// }
/////////////google map//////////

let timeInterval = 0
let distanceInterval = 0
let hikedCoordinates = []
let hikedDistance = 0
let hikingPace = 0

const haveImg = function (propic) {
    if (propic) {
        return `../uploads/${propic}`;
    } else {
        return '../uploads/inv_misc_questionmark.jpg';
    }
};

async function loadActivityInfo() {
    const resActivity = await fetch(`/activity_details/${activityID}`);
    const activityInfo = (await resActivity.json()).rows[0]
    console.log(activityInfo)
    let guildOrNot = ''
    if (activityInfo['guild_id'] != 0) {
        guildOrNot = '(Guild)'
    }
    document.querySelector(".team_name").innerHTML = `<div class="back"><i class="fas fa-angle-left"></i></div>` + activityInfo.nickname + "'s Team " + guildOrNot
    const resParticipant = await fetch(`/team_participant/${activityID}`)
    const participants = (await resParticipant.json()).rows

    document.querySelector('.participants').innerHTML = `<div class="background" style="background-image: url('../route_pic/${activityInfo.name}.jpg');background-size:100% auto;background-position:center">
                                                            <div class="leader"></div>
                                                            <div class="large-icons-container">
                                                                <div class="icons-container">
                                                                    <div id="row1" class="icons-row"></div>
                                                                </div>
                                                            </div>
                                                            <div class="request no_request">
                                                                <div class="request_text">REQUEST</div>
                                                                <div class="request-icons-container">
                                                                    <div id="request-updating" class="request_icons"></div>
                                                                </div>
                                                            </div>
                                                        </div>`

    requestColor()

    document.querySelector('.request_text').addEventListener('click', function (event) {
        let container = event.currentTarget.parentNode.classList
        if (container.contains('request_open')) {
            container.remove('request_open')
        } else {
            container.add('request_open')
        }
    })

    loadActivityRequest()




    let leader_icon_inner = `<div class="user-icon" userID=${activityInfo.leader_id} style="background-image: url(${haveImg(activityInfo.profilepic)})">
                                <div class="user-name">${activityInfo.nickname}</div>
                            </div>`
    document.querySelector('.leader').innerHTML = leader_icon_inner
    let participant_icons_inner = ''
    for (let participant of participants) {
        participant_icons_inner += `<div class="user-icon" userID=${participant.id} style="background-image: url('${haveImg(participant.profilepic)}')">
                                        <div class="user-name"><span class="highlight">&nbsp${participant.nickname}&nbsp;</span></div>
                                    </div>`
    }
    let available_teammate = activityInfo.team_size - participants.length
    console.log(available_teammate)
    for (let i = 0; i < available_teammate - 1; i++) {
        participant_icons_inner += `<div class="user-icon" style="background:grey;opacity:0.7">
                                        <div class="user-empty">Empty</div>
                                    </div>`
    }
    document.querySelector('.icons-row').innerHTML = participant_icons_inner
    document.querySelector('.route_info').innerHTML = `<div class="route_name">${activityInfo.name}</div>
                                                        <div class="route_button" routeID="${activityInfo.route_id}">view<br>route</div>`
    document.querySelector('.route_info').classList.add(`b${parseInt(activityInfo.difficulty)}`)
    document.querySelector('.leader').classList.add(`b${parseInt(activityInfo.difficulty)}`)
    document.querySelector('.activity-setting').classList.add(`b${parseInt(activityInfo.difficulty)}`)
    let viewRoute = document.querySelector(".route_button").addEventListener('click', function (event) {
        window.location = (`/route/route.html?routeID=${event.currentTarget.getAttribute('routeID')}`)
    })
    let splice = activityInfo['date'].split(" ")
    let date = splice[0]
    let time = splice[1].split(":")
    let hour = time[0]
    let minute = time[1]
    let timeText = ''
    // console.log(splice[1])
    if (parseInt(hour) > 12) {
        timeText = `${parseInt(hour) - 12}:${minute} pm`
    } else {
        timeText = `${hour}:${minute} am`
    }
    // document.querySelector('.activity-setting').innerText = `Date: ${date} Time: ${time}`
    document.querySelector('.activity-setting').innerHTML = `<table style="width:100%">
                                                                <tr>
                                                                    <td width="40%"><i class="fas fa-calendar-week"></i>: ${date}</th>
                                                                    <td></th>
                                                                    <td width="40%"><i class="fas fa-clock"></i>: ${timeText}</th>
                                                                </tr>
                                                            </table>
                                                            <div class="commence">Start</div>`
    checkHiking()

    startButtonListener()


    document.querySelector('.description').innerText = activityInfo.description
    document.querySelector('.description-form').addEventListener('submit', async function (event) {
        event.preventDefault();
        console.log(event.currentTarget.content.value)
        const res = await fetch(`/activity_description/${activityID}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                content: event.currentTarget.content.value
            })
        })
        const result = await res.json();
        if (res.status == 200 && result.success) {
            console.log(result.message)
        } else {
            alert("Update failed " + result.message);
        }
    })

    let routeCoordinates = []
    routeJson = activityInfo.coordinates
    for (var j in routeJson) {
        routeCoordinates.push([routeJson[j].longitude, routeJson[j].latitude])
    }
    loadMapBox(routeCoordinates)

    let directions = document.querySelectorAll(".direction")
    for (direction of directions) {
        direction.addEventListener('click', function (event) {
            // let location =  getLocation()
            // let currentLocation = [location.coords['latitude'],location.coords['longitude']]
            if (event.currentTarget.innerText == "Start") {
                drawDirection(routeCoordinates, routeCoordinates[0]) //start of hiking route
            } else { //end of hiking route
                drawDirection(routeCoordinates, routeCoordinates[routeCoordinates.length - 1])
            }
        })
    }
    // console.log(JSON.stringify(participants) + "participants")

    // let participantList=''
    // for(let key in activityInfo.participant_id){
    //     participantList+='0'+activityInfo.participant_id[key]
    // }
    // participantList=participantList.substring(1,participantList.length)
    // const resUser = await fetch(`/userInfo/${participantList}`);
    // const userInfo = (await resUser.json()).rows[0]
    // console.log(JSON.stringify(userInfo)+"!!")
    // document.querySelector(".icons-row").innerHTML = activityInfo.nickname + "'s Team"
    document.querySelector(".back").addEventListener('click', async function () {
        const res = await fetch("/index");
        if (res.status == 200) {
            window.history.back();
        }
    })

}

async function checkHiking() {
    const resHiking = await fetch(`/get_activity_status_details/${activityID}`);
    const activityStatusInfo = (await resHiking.json()).rows[0]
    const startTimeInfo = activityStatusInfo['start_time']
    // console.log(activityStatusInfo)
    let startTime = startTimeInfo.split('T')[1]
    let startHour = parseInt(startTime.split(":")[0]) + 8
    let startMin = startTime.split(":")[1]
    let startTimeString = ''
    if (startHour >= 24) {
        startHour -= 24
        startTimeString = startHour + " : " + startMin + " am"
    } else if (startHour > 12) {
        startHour -= 12
        startTimeString = startHour + " : " + startMin + " pm"
    } else {
        startTimeString = startHour + " : " + startMin + " am"
    }
    let activityStatus = activityStatusInfo['status']
    // let durationString = activityStatusInfo['duration']['minutes']+" : "+activityStatusInfo['duration']['seconds']
    let distance = activityStatusInfo['hiked_distance']

    let startButton = document.querySelector('.commence')
    document.querySelector('.hiking-info table tr th:first-child').innerHTML = `<span class="large">${startTimeString}</span><br>Start`
    switch (activityStatus) {
        case "hiking":
            startButton.innerText = 'Stop'
            startButton.classList.add('hiking')
            let totalSeconds = 0
            if (activityStatusInfo['duration']['minutes']) {
                totalSeconds = parseInt(activityStatusInfo['duration']['minutes']) * 60 + parseInt(activityStatusInfo['duration']['seconds'])
            } else {
                totalSeconds = parseInt(parseInt(activityStatusInfo['duration']['seconds']))
            }
            timeInterval = setInterval(function () {
                ++totalSeconds;
                second = pad(totalSeconds % 60);
                minute = pad(parseInt(totalSeconds / 60));
                // hour = pad(parseInt((totalSeconds/60)/60));
                document.querySelector('.hiking-info table tr th:nth-child(2)').innerHTML = `<span class="large">${minute} : ${second}</span><br>Duration`
                startButton.classList.toggle('red')
            }, 1000);
            break;
        case "end":
            startButton.innerText = 'Finished'
            startButton.classList.add('hiked')
            let durationText = ''
            if (activityStatusInfo['duration']['minutes']) {
                durationText = activityStatusInfo['duration']['minutes'] + " : " + activityStatusInfo['duration']['seconds']
            } else {
                durationText = "00 : " + (activityStatusInfo['duration']['seconds'])
            }
            document.querySelector('.hiking-info table tr th:nth-child(2)').innerHTML = `<span class="large">${durationText}</span><br>Duration`
            break;
    }
    startButtonListener()
}

document.querySelector("#button_home").addEventListener('click', function () {
    window.location = `/index/index.html`;
})

async function loadActivityRequest() {
    const resRequest = await fetch(`/request_team/${activityID}`);
    const requests = (await resRequest.json()).rows
    let requestInner = ''
    // console.log(requests[0])
    // var requestCount=$(".request_icons").find(".request-field").length;
    // var requestCount=document.querySelectorAll('.request-field').length
    // console.log(requestCount+"!!")

    try {
        for (let request of requests) {
            requestInner += `<div class="request-field">
                            <div class="user-icon" style="background-image: url(${haveImg(request.profilepic)})">
                                <div class="user-name-request">${request['nickname']}</div>
                            </div>
                            <div class="accept">
                                <i class="fas fa-check acceptIcon" acceptID=${request['id']} activityID=${request['activity_id']}></i>
                                <i class="fas fa-times rejectIcon" rejectID=${request['id']}></i>
                            </div>
                        </div>`
        }
    } catch (err) {
        console.log("no request")
    }

    document.querySelector('.request_icons').innerHTML = requestInner
    let accepts = document.querySelectorAll('.acceptIcon')
    for (accept of accepts) {
        accept.addEventListener('click', async function (event) {
            const formData = new FormData();
            formData.requestID = event.currentTarget.getAttribute('acceptID')
            formData.joinActivityID = event.currentTarget.getAttribute('activityID')

            const res = await fetch('/accept_team_participant', {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(formData)
            });
            if (res.status === 200) {
                // console.log(await res.json())
            }
            loadActivityInfo()
            var requestCount = document.querySelectorAll('.request-field').length
            // console.log(requestCount)
            if (requestCount > 1) {
                loadActivityRequest()
            } else {
                document.querySelector('.request_icons').innerHTML = ''
            }
            requestColor()
        })
    }
    let rejects = document.querySelectorAll('.rejectIcon')
    for (reject of rejects) {
        reject.addEventListener('click', async function (event) {
            const formData = new FormData();
            formData.requestID = event.currentTarget.getAttribute('rejectID')

            const res = await fetch('/reject_team_participant', {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(formData)
            });
            if (res.status === 200) {
                // console.log(await res.json())
            }
            var requestCount = document.querySelectorAll('.request-field').length
            // console.log(requestCount)
            if (requestCount > 1) {
                loadActivityRequest()
            } else {
                document.querySelector('.request_icons').innerHTML = ''
            }
            requestColor()
        })
    }
    requestColor()
}



function requestColor() {
    var parent = document.getElementById("request-updating");
    let count = 0
    try {
        count = parent.getElementsByClassName('request-field').length
    } catch (err) { }
    if (count === 0) {
        document.querySelector('.request').classList.add('no_request')
    } else {
        document.querySelector('.request').classList.remove('no_request')
    }

}

async function startButtonListener() {
    document.querySelector('.commence').addEventListener('click', async function (event) {
        let totalSeconds = 0
        let minute = 0
        let second = 0
        if (event.currentTarget.classList.contains('hiking')) {
            event.currentTarget.innerText = 'Finished'
            event.currentTarget.classList.add('hiked')
            clearInterval(timeInterval)
            clearInterval(distanceInterval)
            console.log(activityID + "activityID")
            const res = await fetch(`/activity_ended/${activityID}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    distance: hikedDistance
                })
            })
            const result = await res.json();
            if (res.status == 200 && result.success) {
                // loadMemos();//  不一定要係呢個位再load ，不過再load 就易做啲
            } else {
                alert("Update failed " + result.message);
            }
        } else {
            let currentdate = new Date
            let startTime = currentdate.toLocaleTimeString()
            let startHour = startTime.split(":")[0]
            let startMin = startTime.split(":")[1]
            let startTimeString = ''
            if (startHour > 12) {
                startHour -= 12
                startTimeString = startHour + " : " + startMin + " pm"
            } else {
                startTimeString = startHour + " : " + startMin + " am"
            }
            event.currentTarget.innerText = 'Stop'
            event.currentTarget.classList.add('hiking')
            document.querySelector('.hiking-info table tr th:first-child').innerHTML = `<span class="large">${startTimeString}</span><br>Start`
            // document.querySelector('.hiking-info table tr th:nth-child(2)').innerHTML = `<span class="large">00 : 00</span><br>Duration`

            timeInterval = setInterval(function () {
                ++totalSeconds;
                second = pad(totalSeconds % 60);
                minute = pad(parseInt(totalSeconds / 60));
                // hour = pad(parseInt((totalSeconds/60)/60));
                document.querySelector('.hiking-info table tr th:nth-child(2)').innerHTML = `<span class="large">${minute} : ${second}</span><br>Duration`
            }, 1000);

            const res = await fetch(`/activity_start/${activityID}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    distance: 'start'
                })
            })
            const result = await res.json();
            if (res.status == 200 && result.success) {
                // loadMemos();//  不一定要係呢個位再load ，不過再load 就易做啲
            } else {
                alert("Update failed " + result.message);
            }

            distanceInterval = setTimeout(async function () {
                console.log('pace')
                let geoLocation = await getLocation()
                currentLocation = [geoLocation.coords['longitude'], geoLocation.coords['latitude']]
                console.log(currentLocation + "@@")
                hikedCoordinates.push(currentLocation)
                if (hikedCoordinates.length > 1) {
                    hikedDistance += getDistanceFromLatLonInKm(
                        hikedCoordinates[hikedCoordinates.length - 1][1], hikedCoordinates[hikedCoordinates.length - 1][0]
                        , hikedCoordinates[hikedCoordinates.length][1], hikedCoordinates[hikedCoordinates.length][0])
                }
                totalPaceSeconds = (hikedDistance / (minute * 60 + second))
                paceSecond = pad(totalPaceSeconds % 60);
                paceMinute = pad(parseInt(totalPaceSeconds / 60));
                hikingPace = `${paceMinute} : ${paceSecond}`
                document.querySelector('.hiking-info table tr th:nth-child(3)').innerHTML = `<span class="large">${hikingPace}</span><br>min/m`
                document.querySelector('.hiking-info table tr th:nth-child(4)').innerHTML = `<span class="large">${hikedDistance} km</span><br>Distance`
            }, 10000)

        }
    })
}

////////////////mapBox////////////
function loadMapBox(route, route2) {
    mapboxgl.accessToken = 'pk.eyJ1IjoianMxMzk0MzciLCJhIjoiY2ttcnh0NGsyMGNkYTJvb3liYjU1a2V3cyJ9.BXthxGVcKZtAudZFTfovWA';
    var map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v11',
        center: route[Math.floor(route.length / 2)],//lng,lat
        zoom: 12
    });
    map.on('load', function () {
        map.addSource('route', {
            'type': 'geojson',
            'data': {
                'type': 'Feature',
                'properties': {},
                'geometry': {
                    'type': 'LineString',
                    'coordinates': route
                }
            }
        });
        map.addSource('route2', {
            'type': 'geojson',
            'data': {
                'type': 'Feature',
                'properties': {},
                'geometry': {
                    'type': 'LineString',
                    'coordinates': route2
                }
            }
        });
        map.addLayer({
            'id': 'route',
            'type': 'line',
            'source': 'route',
            'layout': {
                'line-join': 'round',
                'line-cap': 'round'
            },
            'paint': {
                'line-color': 'blue',
                'line-width': 5,
                'line-opacity': 0.5
            }
        });
        // Add geolocate control to the map.
        map.addLayer({
            'id': 'route2',
            'type': 'line',
            'source': 'route2',
            'layout': {
                'line-join': 'round',
                'line-cap': 'round'
            },
            'paint': {
                'line-color': 'red',
                'line-width': 5,
                'line-opacity': 0.5
            }
        });
    });


    var geolocate = new mapboxgl.GeolocateControl({
        positionOptions: {
            enableHighAccuracy: true
        },
        trackUserLocation: true
    });
    // Add the control to the map.
    map.addControl(geolocate);
    // map.on('load', function () {
    //     geolocate.trigger();
    // });
}
//////////////mapBox////////

async function getLocation() {
    return new Promise((res, rej) => {
        navigator.geolocation.getCurrentPosition(res, rej)
    })
}

async function getDirection(start, end) { //lng,lat
    var res = await fetch(`https://api.mapbox.com/directions/v5/mapbox/walking/${start[0]},${start[1]};${end[0]},${end[1]}?geometries=geojson&access_token=pk.eyJ1IjoianMxMzk0MzciLCJhIjoiY2ttcnh0NGsyMGNkYTJvb3liYjU1a2V3cyJ9.BXthxGVcKZtAudZFTfovWA`)
    var direction = await res.json();
    // const notification = document.querySelector('#head_info');
    // console.log(JSON.stringify(direction) + "direction")
    return direction;
}


async function drawDirection(route1, route2_destination) {
    let location = await getLocation()
    currentLocation = [location.coords['longitude'], location.coords['latitude']]
    let directions = await getDirection(currentLocation, route2_destination)//lng,lat
    let currentToDestination = directions['routes'][0]['geometry']['coordinates']
    loadMapBox(route1, currentToDestination)
}


function pad(val) {
    var valString = val + "";
    if (valString.length < 2) {
        return "0" + valString;
    } else {
        return valString;
    }
}

function getDistanceFromLatLonInKm(
    lat1,
    lon1,
    lat2,
    lon2
) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2 - lat1); // deg2rad below
    var dLon = deg2rad(lon2 - lon1);
    var a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(deg2rad(lat1)) *
        Math.cos(deg2rad(lat2)) *
        Math.sin(dLon / 2) *
        Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d;
}

async function loadUserInfo() {
    const nameContainer = document.querySelector("#head_info");
    const bottomContainer = document.querySelector(".bottom_button")
    const res = await fetch("/index");
    const result = await res.json();
    if (res.status === 200) {
        data = result;
        // nameContainer.innerHTML = `<h1>${data.id}+${data.nickname}</h1>`;
        bottomContainer.innerHTML = `${data.nickname}`
        return [parseInt(data.id), (data.nickname + ""), parseInt(data.guild_id)]
    } else {
        console.log('no info')
    }
}

loadUserInfo()