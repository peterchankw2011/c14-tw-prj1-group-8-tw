const chatForm = document.querySelector(".chat-form");
const messageSpace = document.querySelector(".message-space");
document.querySelector(".return").addEventListener("click", () => {
  window.history.back();
});

let ownId = "";
const idFromURL = document.location.href.split("?")[1].split("=")[1];

const haveProPic = function (propic) {
  if (propic) {
    return `../uploads/${propic}`;
  } else {
    return '../uploads/inv_misc_questionmark.jpg'
  }
};

async function loadChat() {
  const res = await fetch("/getChatHistory");
  const result = await res.json();
  const messageSpace = document.querySelector(".message-space");
  const headerName = document.querySelector(".headerName");
  if (res.status === 200) {
    data = result;
    headerName.innerHTML = `${data.guildName.rows[0].name}`;
    for (let i = 0; i < data.chatHistory.rows.length; i++) {
      if (data.chatHistory.rows[i].id === data.id) {
        messageSpace.innerHTML += `<div class="user">
        <div class="user-msg">
         
        ${data.chatHistory.rows[i].content}
          <p class="time">${data.chatHistory.rows[i].created_at}</p>
        </div>
      </div>`;
      } else {
        messageSpace.innerHTML += `<div class="counterpart">
        <div class="chat-pic-small" style="background-image: url(${haveProPic(data.chatHistory.rows[i].profilepic)})"></div>
        <div class="counterpart-msg">
        ${data.chatHistory.rows[i].nickname}: <br />
        ${data.chatHistory.rows[i].content}
          <p class="time">${data.chatHistory.rows[i].created_at}</p>
        </div>
        </div>`;
      }
      messageSpace.scrollTop = messageSpace.scrollHeight;
    }
  }
}
loadChat();

const socket = io();
//get id
socket.on("id", (id) => {
  console.log(id);
  ownId = id;
});

socket.emit("joinRoom", idFromURL);

socket.on("message", (message) => {
  console.log(`this is ownId: ${ownId}`);
  console.log(message.username);
  if (message.username === ownId) {
    outputMessage(message);
  } else {
    outputMessageFromCounterpart(message);
  }

  //scroll down auto
  messageSpace.scrollTop = messageSpace.scrollHeight;
});

//chat Form
chatForm.addEventListener("submit", async (e) => {
  e.preventDefault();

  const msg = e.target.elements.msg.value;

  if (!msg) {
    return false;
  }
  const res1 = await fetch("/index");
  const result1 = await res1.json();
  const nickname = result1.nickname;
  const profilepic = result1.profilepic;

  const res = await fetch("/sendChat", {
    method: "POST",
    headers: {
      "Content-type": "application/json",
    },
    body: JSON.stringify({ msg: msg }),
  });

  if (res.status === 200) {
    const result = await res.json();
    console.log(result);
  }

  //emit message to server
  socket.emit("chatMessage", { msg, nickname, profilepic });

  //clear input
  e.target.elements.msg.value = "";
  e.target.elements.msg.focus();
});

function outputMessage(message) {
  const div = document.createElement("div");
  div.classList.add("user");
  div.innerHTML = `<div class="user")">
  <div class="user-msg" >
    ${message.text}
    <p class="time">${message.time}</p>
  </div>
</div>`;
  messageSpace.appendChild(div);
}

function outputMessageFromCounterpart(message) {
  const div = document.createElement("div");
  div.classList.add("counterpart");
  div.innerHTML = ` 
    <div class="chat-pic-small"  style="background-image: url(${haveProPic(message.profilepic)})"></div>
    <div class="counterpart-msg">
    ${message.nickname}: <br />
      ${message.text}
      <p class="time">${message.time}</p>
    </div>
  `;
  messageSpace.appendChild(div);
}
