document.querySelector(".log").addEventListener("click", () => {
  window.location = "/login/login.html";
});

document
  .querySelector("#register-form")
  .addEventListener("submit", async (event) => {
    event.preventDefault();

    const form = event.target;
    const registerInfo= new FormData();
    registerInfo.append("email", form.email.value)
    registerInfo.append("password", form.password.value)
    registerInfo.append("nickname", form.nickname.value)

    if (form.profilepic.files[0]) {
      registerInfo.append("profilepic", form.profilepic.files[0]);
    }

    if (
      !form.confirmPassword.value ||
      !form.password.value ||
      !form.nickname.value ||
      !form.email.value
    ) {
      document.querySelector(".formHeading").innerHTML = ` <h1>Register</h1>`;
      const h3 = document.createElement("h3");
      h3.classList.add("wrongNotice");
      h3.innerHTML = `Incomplete form`;

      document.querySelector(".formHeading").appendChild(h3);
      console.log("info missing");
      return false;
    }
    if (form.password.value !== form.confirmPassword.value) {
      document.querySelector(".formHeading").innerHTML = ` <h1>Register</h1>`;
      const h3 = document.createElement("h3");
      h3.classList.add("wrongNotice");
      h3.innerHTML = `password and confirm password do not match`;

      document.querySelector(".formHeading").appendChild(h3);
      console.log("not match");
      return false;
    }

    const res = await fetch("/register", {
      method: "POST",
      body: registerInfo,
    });

    if (res.status === 200) {
      const result = await res.json();
      console.log(result);
      window.location = "/register/successreg.html";
    }

    form.reset();
  });
