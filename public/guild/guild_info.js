async function loadUserInfo() {
  const res = await fetch("/index");
  if (res.status === 200) {
    const result = await res.json();
    if(!result.guild_id){
      window.location= "/guild/guild.html";
    }
    return result
  }
}


document.querySelector(".return").addEventListener("click", async () => {
  const res = await fetch("/index");
  if (res.status == 200) {
    window.history.back();
  }
});

document.querySelector("#button_home").addEventListener("click", async () => {
  const res = await fetch("/index");
  if (res.status == 200) {
    window.location = "/index/index.html";
  }
});

async function loadGuildInfo() {
  const guild_name = document.querySelector('#guild-container');

  await loadUserInfo();
  const res = await fetch(`/guild_info`);
  const result = await res.json();
  //console.log(result)

  if (res.status === 200) {
    data = result;
    
    const haveImg = function () {
      if (data.guild_info.icon) {
        return `<img src="../uploads/${data.guild_info.icon}" class="img-fluid" alt="guild profile picture">`;
      } else {
        return `<img src="../uploads/inv_misc_questionmark.jpg" class="img-fluid" alt="guild profile picture">`;
      }
    };

    guild_name.innerHTML = `<div class="guild-img-container">
    ${haveImg()}
  </div>
  <div id="guild-name">${data.guild_info.name}</div>
  <div id="guild-description">${data.guild_info.description}</div>
  <div class="exp-bar">
    <div>等級 ${data.guild_level}</div>
      <div class="meter">
      <div style="width: ${data.guild_current_exp / 100}%"></div>
      <span id="current-exp"> ${data.guild_current_exp}/10000</span>
    </div>
    </div>
    <div class="guild-function">
    <div class="button-container">
    <button type="button" id="guild_create_activity"></button>
    <span class ="button-content">創建活動</span>
    </div>
    <div class="button-container">
    <button type="button" id="guild_activity" ></button>
    <span class ="button-content">公會組隊</span>
    </div>
    <div class="button-container">
    <button type="button" id="guild_chat"></button>
    <span class="button-content">公會頻道</span>
    </div>
    </div>
    <div id="guild_request_container"> <div id="request_title">公會申請</div><div id="requests_container_inner"></div></div>
    `;

    document.querySelector("#guild_create_activity").addEventListener("click", async () => {
      window.location = `/route_menu/route_menu.html`
    });

    document.querySelector("#guild_chat").addEventListener("click", async () => {
      window.location = `/directmsg/directmsg.html?id=${data.guild_info.id}`
    });

    document.querySelector("#guild_activity").addEventListener("click", async () => {
      window.location = `/guild_activity/guild_activity.html`
    });

    //handle guild_leader request tab
    const guild_request = document.querySelector('#guild_request_container');
    const guild_request_inner = document.querySelector('#requests_container_inner');

    if (data.guild_leader) {
      const res = await fetch(`/guild_request_list/${data.guild_info.id}`);
      const result = await res.json();

      if (result.guild_request_list[0]) {
        for (request of result.guild_request_list) {
          const haveImg = function () {
            if (request.profilepic) {
              return `<img src="../uploads/${request.profilepic}" class="img-fluid propic" alt="guild profile picture">`;
            } else {
              return `<img src="../uploads/inv_misc_questionmark.jpg" class="img-fluid propic" alt="guild profile picture">`;
            }
          };

          guild_request_inner.innerHTML += `<div class="request" reqID=${request.id}>
          ${haveImg()}
          <span class="request-nickname">${request.nickname}</span>
          <div class="tick"><i class="fas fa-check"></i></div>
          <div class="cross"><i class="fas fa-times"></i></div>
          </div>`
        }

        const records = document.querySelectorAll(".request");
        for (record of records) {
          const reqID = record.getAttribute("reqID");

          //post request for acceptance
          record.querySelector('.fa-check').addEventListener("click", async () => {
            const res = await fetch(`/guild_request_handle/${reqID}`, {
              method: "POST",
              headers: {
                "Content-type": "application/json"
              },
              body: JSON.stringify({ accept: true })
            });
            const result = await res.json();
            loadGuildInfo();
          });
          //post request for rejection
          record.querySelector('.fa-times').addEventListener("click", async () => {
            const res = await fetch(`/guild_request_handle/${reqID}`, {
              method: "POST",
              headers: {
                "Content-type": "application/json"
              },
              body: JSON.stringify({ accept: false })
            });
            const result = await res.json();
            loadGuildInfo();
          });
        }

        document.querySelector('#request_title').addEventListener("click", () => {
          guild_request.classList.toggle("open")
        })

      }
      else {
        guild_request.innerHTML = `<div id="no_request">沒有申請</div>`
      }
    } else {
      guild_request.remove();
    }



    //Load guild members info
    const guild_members = document.querySelector('#members');
    guild_members.innerHTML = "";

    for (member of data.guild_members) {
      guild_members.innerHTML += `<tr>
      <td class="position">${member.position}</td>
      <td>${member.nickname}</td>
      <td>${Math.floor(member.experience/1000)}</td>
      <td>${member.contribution}</td>
      <td>線上</td>
    </tr>`
    }
  }
}

loadGuildInfo();
