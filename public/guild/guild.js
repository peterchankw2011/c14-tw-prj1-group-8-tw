async function loadUserInfo() {
  const nameContainer = document.querySelector(".getName");
  const res = await fetch("/index");
  const result = await res.json();
  if (res.status === 200) {
    if (!(result.guild_id == null)) {
      window.location = "/guild/guild_info.html"
    }
  }
}

loadUserInfo();


document.querySelector(".return").addEventListener("click", async () => {
  const res = await fetch("/index");
  if (res.status == 200) {
    window.history.back();
  }
});

document.querySelector("#button_home").addEventListener("click", async () => {
  const res = await fetch("/index");
  if (res.status == 200) {
    window.location = "/index/index.html";
  }
});





async function loadGuildList() {
  const guild_list = document.querySelector('#guild-list');
  guild_list.innerHTML = "";

  const res = await fetch("/guild");
  const result = await res.json();

  const res_check_req = await fetch("/guild_request_check")
  const result_check_req = await res_check_req.json();

  let guild_leader = {};
  for (guild_nickname of result.guild_nickname){
    guild_leader[guild_nickname.guild_id] = guild_nickname.nickname
  }

  if (res.status === 200) {
    for (guild of result.guild_list) {
      guild_list.innerHTML += `<tr>
        <td>${guild.name}</td>
        <td>${guild_leader[guild.id]}</td>
        <td>${guild.count}</td>
        <td><button guild_id=${guild.id} type="button" class="btn btn-link">加入</button></td>
      </tr>`
    }
    if (!!result_check_req[0]) {
      for (check_req of result_check_req) {
        document.querySelector(`[guild_id = "${check_req.guild_id}"]`).classList.add("requested")
        document.querySelector(`[guild_id = "${check_req.guild_id}"]`).innerHTML = `已申請`
      }
    }

    const req_btn = document.querySelectorAll(`[guild_id]`);
    for (button of req_btn) {
      const guild_id = button.getAttribute("guild_id")
      button.addEventListener("click", async () => {
        const res = await fetch(`/guild_request/${guild_id}`);
        if (res.status === 200) {
          const result = await res.json();
          alert(result.message);
          loadGuildList();
        }
      })
    }
  }
};


loadGuildList();


document.querySelector("#guild-create-form").addEventListener("submit", async (event) => {
  event.preventDefault();

  const form = event.target;
  const formData = new FormData();
  formData.append("guild_name", form.guild_name.value)
  formData.append("guild_description", form.guild_description.value)
  if (form.img.files[0]) {
    formData.append("guild_image", form.img.files[0]);
  }

  console.log(formData)

  if (form.guild_name.value.trim() === "") {
    alert('公會名稱不能留白');
    form.reset()
    return false;
  } else {
    const res = await fetch("/guild_create", {
      method: "POST",
      body: formData,
    });

     if (res.status === 200) {
      const result = await res.json();
      //console.log(result);
      window.location = "/guild/guild_info.html";
    } 
    form.reset();
  }
});

//modal for create guild
const modal = document.querySelector("#CreateGuildModal");
const create_btn = document.querySelector("#create_guild_btn");
const close_btn = document.querySelectorAll(".close");

create_btn.addEventListener("click", function(event){
  modal.style.display = "block"
})

for(btn of close_btn){
  btn.addEventListener("click", function(event){
    modal.style.display = "none"
  })

}

window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

