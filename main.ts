import express from "express";
import { Request, Response } from "express";
import expressSession from "express-session";
import path from "path";
import { Client, QueryResult } from "pg";
import dotenv from "dotenv";
import http from "http";
import { Server as SocketIO } from "socket.io";
import { formatMessage } from "./utils/message";
import bodyParser from "body-parser";

import { hashPassword, checkPassword } from "./hash";
import multer from "multer";

//connect to own SQL by username and password of .env
dotenv.config();
//multer
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.resolve("./public/uploads"));
  },
  filename: function (req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`);
  },
});
const upload = multer({ storage });

export const client = new Client({
  database: process.env.DB_NAME,
  user: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
});

//setting up socketio
const app = express();
const server = new http.Server(app);
const io = new SocketIO(server);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(
  expressSession({
    secret: "Hiking is fun",
    resave: true,
    saveUninitialized: true,
  })
);
//chat functionality

io.on("connection", function (socket) {
  console.log(socket.id);
  socket.emit("id", socket.id);
  socket.emit("connected", "I am connected");
  socket.on("joinRoom", (idFromURL) => {
    socket.join(idFromURL);
    socket.on("request", (msg) => {
      io.to(idFromURL).emit(msg);
    })
    socket.on("chatMessage", (msg) => {
      io.to(idFromURL).emit(
        "message",
        formatMessage(msg.nickname, socket.id, msg.msg, msg.profilepic)
      );
    });
  });

});

// Initialize session key

app.get("/", function (req: Request, res: Response) {
  res.end("Hello World");
});

app.post("/register", upload.single("profilepic"), async (req, res) => {
  const reg = req.body;
  const hashedPassword = await hashPassword(reg.password);

  if (req.file) {
    reg.profilepic = req.file.filename;
  }

  await client.query(
    `INSERT INTO users (email,password,nickname,profilepic,experience) VALUES ($1,$2,$3,$4,$5)`,
    [reg.email, hashedPassword, reg.nickname, reg.profilepic, 0]
  );
  
  res.json({ success: true }); 
}); 

interface User {
  email: string;
  password: string;
}


interface Activity {
  guild: number;
  public: number;
  routeID: number;
  date: Date;
  description: string;
  teamsize: number;
  center_coordinate: JSON;
}

/* const isLoggedInAPI = (req: Request, res: Response, next: express.NextFunction) => {
    if (req.session['user']) {
        next()
    } else {
        res.status(401).json({ message: "Unauthorized" })
    }
} */

app.post("/login", async (req, res) => {
  const users: User[] = (
    await client.query(`SELECT * from users where email = $1`, [req.body.email])
  ).rows;

  const userFound = users[0];

  if (!userFound) {
    console.log("notfound");
    res
      .status(401)
      .json({ success: false, message: "Incorrect Username/Password" });
    res.redirect("/login/login.html");
    // res.redirect('/login.html?error=Incorrect+Username+Password')
    return;
  }
  const match = await checkPassword(req.body.password, userFound.password);

  if (match) {
    req.session["user"] = userFound;
    res.json({ success: true });
    await client.query(
      `UPDATE users SET last_online= 'online' where users.id = ${req.session["user"].id}`
    );
  } else {
    console.log("incorrect pw");
    // res.redirect('/login.html?error=Incorrect+Username+Password')
    res
      .status(401)
      .json({ success: false, message: "Incorrect Username/Password" });
  }
});

app.get("/logout", async (req, res) => {
  await client.query(
    `UPDATE users SET last_online= NOW() where users.id = ${req.session["user"].id}`
  );
  delete req.session["user"];
  res.json({ success: true });
});

const isLoggedIn = (
  req: Request,
  res: Response,
  next: express.NextFunction
) => {
  if (req.session["user"]) {
    //called Next here
    next();
  } else {
    console.log("user");
    res.redirect("/login/login.html");
    // redirect to index page
  }
};

app.get("/index", isLoggedIn, async function (req, res) {
  const userFoundOld = req.session["user"];
  const userFoundSQL= await client.query(`SELECT * from users where id= $1`, [userFoundOld.id])
  const userFound = userFoundSQL.rows[0];
  req.session["user"] = userFound;

  let guild_name = "";
  if (userFound.guild_id) {
    const guild_nameFound: QueryResult<any> = await client.query(
      `SELECT name FROM guilds WHERE id = ${userFound.guild_id}`
    );
    guild_name = guild_nameFound.rows[0].name;
  }

  if (userFound) {
    res.json({
      id: userFound.id,
      nickname: userFound.nickname,
      profilepic:userFound.profilepic,
      experience: userFound.experience,
      level: Math.floor(userFound.experience / 1000),
      current_exp: userFound.experience % 1000,
      guild_id: userFound.guild_id,
      guildName: guild_name,
    });
  } else {
    //唔suppose 去到呢個位
    res.status(401).json({ message: "Unauthorized" });
  }
});

app.get("/userInfo/:participantList", async (req, res) => {
  const participants = req.params.participantList.split("0");
  let userIDList = "";
  for (let participant of participants) {
    userIDList += "," + participant;
  }
  userIDList = userIDList.substring(1, userIDList.length);
  if (userIDList.length == 1) {
    res.json(
      await client.query(`SELECT * FROM users WHERE id = ${userIDList}`)
    );
  } else {
    res.json(
      await client.query(`SELECT * FROM users WHERE id IN ${userIDList}`)
    );
  }
});

app.get("/loadroutes", async (req, res) => {
  res.json(
    await client.query(
      `SELECT id,name, district, difficulty, dimension FROM routes`
    )
  );
});

app.get("/routeinfo/:routeID", async function (req, res) {
  const routeID = parseInt(req.params.routeID);
  if (isNaN(routeID)) {
    res.status(400).json({ message: "id is not an integer" });
    return;
  }
  res.json(await client.query(`SELECT * FROM routes where id = ${routeID}`));
});

app.post("/allactivities", async function (req, res) {
  const guild_id = req.session["user"].guild_id;
  // const activity: Activity = req.body;
  const reqBody = req.body;
  switch (reqBody.filter) {
    case "Guild":
      let guild_select = await client.query(`SELECT activities.id as activity_id, activities.*, routes.id, routes.district, routes.name, routes.difficulty, users.nickname, users.profilepic FROM activities 
                                            INNER JOIN users 
                                                ON activities.leader_id = users.id
                                            INNER JOIN routes
                                                ON activities.route_id = routes.id
                                            WHERE activities.guild_id = ${guild_id}
                                            ORDER BY routes.district`);
      console.log(guild_select);
      res.json(guild_select);
      return;
    case "District":
      let a = await client.query(`SELECT activities.id as activity_id, activities.*, routes.id, routes.district, routes.name, routes.difficulty, users.nickname, users.profilepic FROM activities 
                                            INNER JOIN users 
                                                ON activities.leader_id = users.id
                                            INNER JOIN routes
                                                ON activities.route_id = routes.id
                                            ORDER BY routes.district`);
      res.json(a);
      return; //need ask question why need return otherwise : cannot set header after they are sent to the client
    case "Nearby":
      let userCenter = req.body.center; //[lng,lat]
      let otherActivitiesID = await client.query(
        `SELECT route_id, center, MAX(id) FROM activities GROUP BY route_id, center`
      );
      let other_routeID = []; //find route within range
      let distance_away = {};
      for (let route of otherActivitiesID.rows) {
        let centerLat = parseFloat(route["center"]["latitude"]);
        let centerLng = parseFloat(route["center"]["longitude"]);
        other_routeID.push(route["route_id"]);
        let awayDistance = getDistanceFromLatLonInKm(
          parseFloat(userCenter[1]),
          parseFloat(userCenter[0]),
          centerLat,
          centerLng
        );
        distance_away[`${route["route_id"]}`] = awayDistance;
      }
      let otherRouteString = "";
      for (let routeID of other_routeID) {
        otherRouteString += `,${routeID}`;
      }
      otherRouteString = otherRouteString.substring(1, otherRouteString.length);
      let select = await client.query(`SELECT activities.*, users.nickname, users.profilepic, routes.name, routes.difficulty FROM activities 
                                        INNER JOIN users 
                                            ON activities.leader_id=users.id 
                                        INNER JOIN routes
                                            ON activities.route_id = routes.id
                                        WHERE activities.route_id in (${otherRouteString})
                                        ORDER BY activities.date`);
      let response = [select, distance_away];
      res.json(response); //select activities of subject and nearby routes
      return;
  }
});

app.get("/count_participants", async function (req, res) {
  res.json(
    await client.query(
      `SELECT activity_id, COUNT(*) FROM participants_activities GROUP BY activity_id `
    )
  );
});

app.get("/activity/:routeID", async function (req, res) {
  const routeID = parseInt(req.params.routeID);
  if (isNaN(routeID)) {
    res.status(400).json({ message: "id is not an integer" });
    return;
  }
  let range = 30; //km
  let rangeLat = range * 0.0113838997;
  let rangeLng = range * 0.0090132964;
  let routeCoordinates = [];
  const selectRoute = await client.query(
    `SELECT coordinates FROM routes WHERE id=${routeID}`
  );
  const subjectRouteJson = selectRoute.rows[0].coordinates;
  for (var j in subjectRouteJson) {
    routeCoordinates.push([
      subjectRouteJson[j].longitude,
      subjectRouteJson[j].latitude,
    ]);
  }
  let subjectCenter = routeCoordinates[Math.floor(routeCoordinates.length / 2)]; //lng,lat
  let maxLat = parseFloat(subjectCenter[1]) + rangeLat;
  let minLat = parseFloat(subjectCenter[1]) - rangeLat;
  let maxLng = parseFloat(subjectCenter[0]) + rangeLng;
  let minLng = parseFloat(subjectCenter[0]) - rangeLng;
  let otherActivitiesID = await client.query(
    `SELECT route_id, center, MAX(id) FROM activities GROUP BY route_id, center`
  );
  let other_routeID = []; //find route within range
  let distance_away = {};
  for (let route of otherActivitiesID.rows) {
    let centerLat = parseFloat(route["center"]["latitude"]);
    let centerLng = parseFloat(route["center"]["longitude"]);
    if (
      centerLat > minLat &&
      centerLat < maxLat &&
      centerLng > minLng &&
      centerLng < maxLng
    ) {
      other_routeID.push(route["route_id"]);
    }
    let awayDistance = getDistanceFromLatLonInKm(
      parseFloat(subjectCenter[1]),
      parseFloat(subjectCenter[0]),
      centerLat,
      centerLng
    );
    distance_away[`${route["route_id"]}`] = awayDistance;
  }
  let otherRouteString = "";
  for (let routeID of other_routeID) {
    otherRouteString += `,${routeID}`;
  }
  otherRouteString = otherRouteString.substring(1, otherRouteString.length);
  let select = await client.query(`SELECT activities.*, users.nickname, users.profilepic, routes.name FROM activities 
                                        INNER JOIN users 
                                            ON activities.leader_id=users.id 
                                        INNER JOIN routes
                                            ON activities.route_id = routes.id
                                            WHERE activities.route_id in (${otherRouteString})`);
  let response = [select, distance_away];
  res.json(response); //select activities of subject and nearby routes
});

app.post("/activity", async function (req, res) {
  const leader_id = req.session["user"].id;
  const guild_id = req.session["user"].guild_id;
  console.log(guild_id + "isguild");
  console.log(guild_id + "guild");
  const activity: Activity = req.body;
  // let dateType = activity.date
  // var now = new Date()
  // console.log(now+"now time")
  // const timeSQL = now.toISOString().slice(0, 19).replace('T', ' ')
  // console.log(timeSQL + "test time")
  let result: QueryResult<any>;
  let participant_id = {};
  participant_id["1"] = leader_id;
  let SQLguildId = 0;
  if (activity.guild == 1) {
    SQLguildId = guild_id;
  }
  result = await client.query(
    `INSERT INTO activities (status, guild_id, public, route_id, leader_id, team_size, date, description, center, participant_id, created_at, updated_at ) 
                        values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10, NOW(),NOW()) returning id`,
    [
      "pending",
      SQLguildId,
      activity.public,
      activity.routeID,
      leader_id,
      activity.teamsize,
      activity.date,
      activity.description,
      activity.center_coordinate,
      participant_id,
    ]
  );
  io.emit("new-activity", activity);
  res.json({ id: result.rows[0].id });
});

app.post("/activity", async function (req, res) {
  const leader_id = req.session["user"].id;
  const guild_id = req.session["user"].guild_id;
  console.log(guild_id + "isguild");
  console.log(guild_id + "guild");
  const activity: Activity = req.body;
  // let dateType = activity.date
  // var now = new Date()
  // console.log(now+"now time")
  // const timeSQL = now.toISOString().slice(0, 19).replace('T', ' ')
  // console.log(timeSQL + "test time")
  let result: QueryResult<any>;
  let participant_id = {};
  participant_id["1"] = leader_id;
  let SQLguildId = 0;
  if (activity.guild == 1) {
    SQLguildId = guild_id;
  }
  result = await client.query(
    `INSERT INTO activities (status, guild_id, public, route_id, leader_id, team_size, date, description, center, participant_id, created_at, updated_at ) 
                        values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10, NOW(),NOW()) returning id`,
    [
      "pending",
      SQLguildId,
      activity.public,
      activity.routeID,
      leader_id,
      activity.teamsize,
      activity.date,
      activity.description,
      activity.center_coordinate,
      participant_id,
    ]
  );
  io.emit("new-activity", activity);
  res.json({ id: result.rows[0].id });
});

app.put("/activity_description/:activityID", async function (req, res){
    console.log('in')
    const updateActivityID = parseInt(req.params.activityID);
    const description = req.body.content
    await client.query(`UPDATE activities set description = $1 where id = $2`,
                            [description,updateActivityID])
    res.json({success: true})

})

app.put("/activity_start/:activityID", async function (req, res) {
  const startActivityID = parseInt(req.params.activityID);
  if (isNaN(startActivityID)) {
    res.status(400).json({ message: "activity id is not an integer" });
    return;
  }
  await client.query(
    `UPDATE activities set status = $1, start_time = NOW(), updated_at = NOW() where id = $2`,
    ["hiking", startActivityID]
  );
  // io.to(`user-${req.session['user'].username}`).emit('memo-updated',{message:"memo-updated!!"});
  res.json({ success: true });
});

app.put("/activity_ended/:activityID", async function (req, res) {
  const endActivityID = parseInt(req.params.activityID);
  const distance = req.body.distance;
  if (isNaN(endActivityID)) {
    res.status(400).json({ message: "activity id is not an integer" });
    return;
  }
  await client.query(
    `UPDATE activities set status = $1, end_time = NOW(), hiked_distance=$2,  updated_at = NOW() where id = $3`,
    ["end", distance, endActivityID]
  );
  // io.to(`user-${req.session['user'].username}`).emit('memo-updated',{message:"memo-updated!!"});

  //Get activity guild status, users id and add experience by difficulty
  let activity_all_participants = [];

  const leaderID_difficulty = await client.query(
    `SELECT activities.id, routes.difficulty, activities.guild_id, activities.status, activities.leader_id, activities.date
    FROM activities, routes
    WHERE activities.route_id = routes.id AND activities.id = $1`,
    [endActivityID]
  );

  activity_all_participants.push(leaderID_difficulty.rows[0].leader_id);

  const experienceAdd = leaderID_difficulty.rows[0].difficulty * 100;
  const guild_id = leaderID_difficulty.rows[0].guild_id;

  const participants = await client.query(
    `SELECT activities.id,  activities.guild_id, activities.status, participants_activities.user_id, activities.date
    FROM participants_activities, activities 
    WHERE participants_activities.activity_id = activities.id AND participants_activities.activity_id = $1`,
    [endActivityID]
  );

  let participant;
  if (!!participants.rows[0]) {
    for (participant of participants.rows) {
      activity_all_participants.push(participant.user_id);
    }
  }

  //add experience to users and related guild
  let user_id;
  for (user_id of activity_all_participants) {
    await client.query(
      `UPDATE users
        SET experience = experience + $1
        WHERE id = $2`,
      [experienceAdd, user_id]
    );
    if (!(guild_id == 0)) {
      let contributionAdd = experienceAdd / 2;
      await client.query(
        `UPDATE guild_user
          SET contribution = contribution + $1
          WHERE guild_id = $2 AND user_id = $3`,
        [contributionAdd, guild_id, user_id]
      );

      await client.query(
        `UPDATE guilds
          SET experience = experience + $1
          WHERE id = $2`,
        [contributionAdd, guild_id]
      );
    }
  }
  res.json({ success: true });
});

app.get("/get_activity_status_details/:activityID", async function (req, res) {
  const checkActivityID = parseInt(req.params.activityID);
  if (isNaN(checkActivityID)) {
    res.status(400).json({ message: "activity id is not an integer" });
    return;
  }
  let status = await client.query(`SELECT status, hiked_distance, start_time,
                                        (NOW() - start_time) AS duration
                                        FROM activities 
                                            WHERE id = ${checkActivityID}`);
  res.json(status);
});

app.get("/activity_details/:activityID", async function (req, res) {
  // const userID = req.session["user"].id
  const activityID = parseInt(req.params.activityID);
  if (isNaN(activityID)) {
    res.status(400).json({ message: "id is not an integer" });
    return;
  }
  let select = await client.query(`SELECT activities.*, users.nickname, users.profilepic, routes.name, routes.difficulty, routes.coordinates FROM activities 
                                        INNER JOIN users 
                                            ON activities.leader_id=users.id 
                                        INNER JOIN routes
                                            ON activities.route_id = routes.id
                                        WHERE activities.id = ${activityID}
                                        ORDER BY activities.date`);
  // let leader_id = select.rows[0].leader_id
  // if (leader_id === userID) {
  res.json(select);
  // } else {
  // res.json(("u are not the leader"))
  // }
});

app.post("/request_team/:activityID", async function (req, res) {
  const userID = req.session["user"].id;
  const activityID = parseInt(req.params.activityID);
  let result: QueryResult<any>;
  if (isNaN(activityID)) {
    res.status(400).json({ message: "id is not an integer" });
    return;
  }

  result = await client.query(
    `INSERT INTO requests_activity (user_id, activity_id, created_at, updated_at ) 
                        values ($1,$2,NOW(),NOW()) returning id`,
    [userID, activityID]
  );

  res.json({ id: result.rows[0].id });
});

app.get("/request_team/:activityID", async function (req, res) {
  const leaderID = req.session["user"].id;
  const activityID = parseInt(req.params.activityID);
  if (isNaN(activityID)) {
    res.status(400).json({ message: "id is not an integer" });
    return;
  }
  let select = await client.query(`SELECT requests_activity.*, activities.leader_id, users.nickname, users.profilepic FROM requests_activity 
                                        INNER JOIN activities 
                                            ON requests_activity.activity_id = activities.id
                                        INNER JOIN users
                                            ON requests_activity.user_id = users.id
                                        WHERE activity_id = ${activityID}`);
  try {
    if (select.rows[0].leader_id) {
      let leader_id = select.rows[0].leader_id; //.leader_id of undefined since no request
      if (leaderID === leader_id) {
        res.json(select);
      } else {
        res.json("u r not leader cant see others' request");
      }
    } else {
      res.json("no request");
    }
  } catch (err) {
    console.log("no request");
    res.json("no request");
  }
});

app.get("/request_check/:activityID", async function (req, res) {
  const userID = req.session["user"].id;
  console.log(userID + "ID");
  const activityID = parseInt(req.params.activityID);
  if (isNaN(activityID)) {
    res.status(400).json({ message: "id is not an integer" });
    return;
  }
  let select = await client.query(
    `SELECT * FROM requests_activity WHERE user_id = ${userID} AND activity_id = ${activityID}`
  );
  // console.log(JSON.stringify(select))
  res.json(select);
});

app.get("/team_participant/:activityID", async function (req, res) {
  const activityID = parseInt(req.params.activityID);
  if (isNaN(activityID)) {
    res.status(400).json({ message: "id is not an integer" });
    return;
  }
  let select = await client.query(`SELECT participants_activities.*, users.nickname, users.profilepic FROM participants_activities
                                        INNER JOIN users 
                                            ON participants_activities.user_id = users.id
                                        WHERE activity_id = ${activityID}`);
  res.json(select);
});

app.get("/check_isHiking", async function (req, res) {
  const userID = req.session["user"].id;
  let leaderActivityId = await client.query(
    `SELECT id FROM activities WHERE leader_id = ${userID} AND status = 'hiking'`
  );
  let participantActivityId = await client.query(`SELECT participants_activities.activity_id FROM participants_activities 
                                                        INNER JOIN activities ON participants_activities.activity_id = activities.id
                                                        WHERE participants_activities.user_id = ${userID}  AND activities.status = 'hiking'`);
  res.json([leaderActivityId.rows, participantActivityId.rows]);
});

app.get("/check_user_is_participant/:activityID", async function (req, res) {
  const userID = req.session["user"].id;
  const activityID = parseInt(req.params.activityID);
  if (isNaN(activityID)) {
    res.status(400).json({ message: "id is not an integer" });
    return;
  }
  let select = await client.query(
    `SELECT COUNT (user_id) FROM participants_activities WHERE activity_id = ${activityID} AND user_id = ${userID}`
  );
  res.json(select);
});

app.post("/accept_team_participant", async function (req, res) {
  const reqBody = req.body; //.requestID, .joinActivityID
  let result: QueryResult<any>;
  const requestInfo = await client.query(
    `SELECT * from requests_activity WHERE id = ${reqBody.requestID}`
  );
  // console.log(JSON.stringify(requestInfo))
  let request_userID = requestInfo.rows[0].user_id;
  let request_activityID = requestInfo.rows[0].activity_id;
  if (parseInt(reqBody.joinActivityID) === request_activityID) {
    let rowsCount = await client.query(
      `SELECT COUNT (activity_id) FROM participants_activities WHERE activity_id = ${reqBody.joinActivityID}`
    );
    let selectTeamSize = await client.query(
      `SELECT team_size FROM activities WHERE id = ${reqBody.joinActivityID}`
    );
    let teamSize = parseInt(selectTeamSize["rows"][0]["team_size"]);
    let no_of_participants = parseInt(rowsCount["rows"][0]["count"]);
    if (no_of_participants === teamSize - 1) {
      res.json("full");
      return;
    } else {
      let delete_request = await client.query(
        `DELETE FROM requests_activity WHERE id = ${reqBody.requestID}`
      );
      result = await client.query(
        `INSERT INTO participants_activities (user_id, activity_id, created_at)
                                        values ($1, $2, NOW()) returning id`,
        [request_userID, request_activityID]
      );
      res.json({ delete_id: delete_request, id: result.rows[0].id });
      return;
    }
  } else {
    let msg = "activity check failed";
    res.json(msg);
    return;
  }
});

app.post("/reject_team_participant", async function (req, res) {
  const reqBody = req.body; //.requestID, .joinActivityID
  let delete_request = await client.query(
    `DELETE FROM requests_activity WHERE id = ${reqBody.requestID}`
  );
  res.json(delete_request);
});

app.get("/nearbyActivity/:routeID", async function (req, res) {
  const routeID = parseInt(req.params.routeID);
  if (isNaN(routeID)) {
    res.status(400).json({ message: "id is not an integer" });
    return;
  }
  res.json(
    await client.query(`SELECT * FROM activities where route_id = ${routeID}`)
  );
});

app.get("/guild", isLoggedIn, async function (req, res) {
  const userFound = req.session["user"];
  //console.log(userFound);
  const guild_list = await client.query(`SELECT guilds.id, guilds.name, COUNT(guild_user.id)
    FROM guilds, guild_user
    WHERE guilds.id = guild_user.guild_id
    GROUP BY guilds.id`);

  const guild_nickname = await client.query(`SELECT  guild_user.guild_id, users.nickname
  FROM users, guild_user
  WHERE users.id = guild_user.user_id AND guild_user.position = '會長'`)

  //console.log(guild_list.rows);

  if (userFound) {
    res.json({
      guild_list: guild_list.rows,
      guild_nickname: guild_nickname.rows
    });
  } else {
    //唔suppose 去到呢個位
    res.status(401).json({ message: "Unauthorized" });
  }
});



app.post("/guild_create", upload.single("guild_image"), async function (req, res) {
  const session_user = req.session["user"];
  const guild_info = req.body;

  if (req.file) {
    guild_info.guild_image = req.file.filename;
  }
  //console.log(guild_info)

  let result: QueryResult<any>;

  result = await client.query(
    `INSERT INTO guilds (name, description, icon, level, experience, created_at, updated_at ) 
                            values ($1,$2,$3,$4,$5, NOW(),NOW()) returning id`,
    [guild_info.guild_name, guild_info.guild_description, guild_info.guild_image, 0, 0]
  );
  await client.query(
    `INSERT INTO guild_user (guild_id, user_id, contribution, position, joined_at ) 
                            values ($1,$2,$3,$4,NOW()) returning id`,
    [result.rows[0].id, session_user.id, 0, "會長"]
  );
  await client.query(
    `UPDATE users
        SET guild_id = $1 WHERE id = $2`,
    [result.rows[0].id, session_user.id]
  );

  req.session["user"].guild_id = result.rows[0].id;

  res.json({ success: true });
});

app.get("/guild_info", isLoggedIn, async function (req, res) {
  const userFound = req.session["user"];
  const guild_info = await client.query(`SELECT * FROM guilds WHERE id = $1`, [
    userFound.guild_id]
  );

  const guild_members = await client.query(
    `SELECT guild_user.user_id, guild_user.position, users.experience, users.nickname, guild_user.contribution
    FROM guild_user
    INNER JOIN users ON guild_user.user_id=users.id AND guild_user.guild_id = $1
    ORDER BY guild_user.contribution DESC, guild_user.position DESC`,
    [userFound.guild_id]
  );

  //Checking
  //console.log(userFound);
  //console.log(guild_info.rows)
  //console.log(guild_members.rows);

  function GetLeaderID() {
    let member;
    for (member of guild_members.rows) {
      if (member.position == "會長") {
        return member.user_id;
      }
    }
  }

  if (userFound) {
    res.json({
      guild_info: guild_info.rows[0],
      guild_level: Math.floor(guild_info.rows[0].experience / 10000),
      guild_current_exp: guild_info.rows[0].experience % 10000,
      guild_members: guild_members.rows,
      guild_leader: userFound.id == GetLeaderID(),
    });
  } else {
    //唔suppose 去到呢個位
    res.status(401).json({ message: "Unauthorized" });
  }
});

app.get("/guild_request_check", isLoggedIn, async function (req, res) {
  const userFound = req.session["user"];
  const guild_list = await client.query(
    `SELECT DISTINCT guild_id from guild_user_request WHERE user_id = $1`,
    [userFound.id]
  );
  res.json(guild_list.rows);
});

app.get("/guild_request/:id", isLoggedIn, async function (req, res) {
  const userFound = req.session["user"];
  const guild_id = parseInt(req.params.id);
  const check_request = await client.query(
    `SELECT id FROM guild_user_request WHERE guild_id = $1 AND user_id = $2 `,
    [guild_id, userFound.id]
  );

  //Checking
  //console.log(userFound);
  //console.log(guild_id)
  //console.log(check_request.rows[0])
  if (!check_request.rows[0]) {
    const guild_request = await client.query(
      `INSERT INTO guild_user_request (
      guild_id,
      user_id,
      request_at
  )
  values ($1, $2, NOW()) returning id;`,
      [guild_id, userFound.id]
    );

    res.json({ message: "Request submitted.", request_id: guild_request });
  } else {
    res.json({ message: "Had already requested" });
  }
});

app.get("/guild_request_list/:id", isLoggedIn, async function (req, res) {
  //const userFound = req.session["user"];
  const guild_id = parseInt(req.params.id);
  //console.log(userFound);
  const check_request = await client.query(
    `SELECT guild_user_request.id, guild_user_request.guild_id, guild_user_request.user_id, users.nickname, users.profilepic
  FROM guild_user_request, users
  WHERE guild_user_request.user_id = users.id AND guild_user_request.guild_id = $1 `,
    [guild_id]
  );
  //console.log(check_request.rows);

  res.json({ guild_request_list: check_request.rows });
});

app.post("/guild_request_handle/:id", isLoggedIn, async function (req, res) {
  //const userFound = req.session["user"];
  const reqID = req.params.id;
  const acceptance = req.body.accept;
  //console.log(userFound);

  if (acceptance) {
    const get_IDs = await client.query(
      `SELECT user_id, guild_id FROM guild_user_request WHERE guild_user_request.id = $1`,
      [reqID]
    );
    await client.query(`DELETE FROM guild_user_request WHERE user_id = $1`, [
      get_IDs.rows[0].user_id,
    ]);
    await client.query(
      `INSERT INTO guild_user (guild_id, user_id, contribution, position, joined_at ) 
    values ($1,$2,$3,$4,NOW()) returning id`,
      [get_IDs.rows[0].guild_id, get_IDs.rows[0].user_id, 0, "會員"]
    );
    await client.query(
      `UPDATE users
            SET guild_id = $1 WHERE id = $2`,
      [get_IDs.rows[0].guild_id, get_IDs.rows[0].user_id]
    );

    req.session["user"].guild_id = get_IDs.rows[0].guild_id;
    res.json({ success: true });

  } else {
    await client.query(`DELETE FROM guild_user_request WHERE id = $1`, [reqID]);
    res.json({ success: true });
  }
});

app.get("/journal", async (req, res) => {
  const userFound = req.session["user"];
  const host_activitiesSQL = await client.query(
    `SELECT activities.id, routes.id AS route_id, routes.name, routes.difficulty, activities.guild_id, activities.status, activities.date
  FROM activities, routes
  WHERE activities.route_id = routes.id AND activities.leader_id = $1`,
    [userFound.id]
  );

  const joined_activitiesSQL = await client.query(
    `SELECT activities.id, routes.id AS route_id, routes.name, routes.difficulty, activities.guild_id, activities.status, activities.date, users.nickname
  FROM participants_activities, activities, routes, users
  WHERE activities.route_id = routes.id AND participants_activities.activity_id = activities.id AND activities.leader_id = users.id AND participants_activities.user_id = $1`,
    [userFound.id]
  );

  res.json({
    host: host_activitiesSQL.rows,
    joined: joined_activitiesSQL.rows,
  });
});

app.get("/allCat", async (req, res) => {
  const allCat = await client.query(`SELECT Distinct district from routes`);
  res.json(allCat);
});
app.get("/getDistrictRoute/:value", async (req, res) => {
  const value = req.params.value;
  const districtRoute = await client.query(`select name, id
  from routes
  where district = '${value}';`);
  res.json(districtRoute);
});

app.get("/allThread", async (req, res) => {
  const allThread = await client.query(/*sql*/ `SELECT threads.id, routes.district, routes.name, threads.title, threads.upvote, threads.created_at, sorted_comments.max_updated_at
    FROM threads
    LEFT JOIN routes on threads.route_id = routes.id
    LEFT JOIN
    (SELECT comments.thread_id AS thread_id ,max(comments.updated_at) AS max_updated_at
    FROM comments 
    GROUP BY comments.thread_id) AS sorted_comments on threads.id = sorted_comments.thread_id
    ORDER BY max_updated_at DESC ;`);

  res.json(allThread);
});

app.get("/filtered/:id", async (req, res) => {
  const id = req.params.id;
  console.log(id);
  const abc = await client.query(
    `SELECT threads.id, routes.district, routes.name, threads.title, threads.upvote, threads.created_at, sorted_comments.max_updated_at
    FROM threads
    LEFT JOIN routes on threads.route_id = routes.id 
    LEFT JOIN
    (SELECT comments.thread_id AS thread_id ,max(comments.updated_at) AS max_updated_at
    FROM comments 
    GROUP BY comments.thread_id) AS sorted_comments on threads.id = sorted_comments.thread_id
    where routes.district='${id}'
    ORDER BY max_updated_at DESC ;`
  );

  res.json(abc);
});

app.get("/chatFilteredThread/:routeID", async (req, res) => {
    const routeID = req.params.routeID;
    let routeDistrict = (await client.query(`SELECT district from routes WHERE id = ${routeID}`)).rows[0].district
    const abc = await client.query(
        `SELECT threads.id, routes.id as route_id, routes.district, routes.name, threads.title, threads.upvote, threads.created_at, sorted_comments.max_updated_at
      FROM threads
      LEFT JOIN routes on threads.route_id = routes.id 
      LEFT JOIN
      (SELECT comments.thread_id AS thread_id ,max(comments.updated_at) AS max_updated_at
      FROM comments 
      GROUP BY comments.thread_id) AS sorted_comments on threads.id = sorted_comments.thread_id
      where routes.district='${routeDistrict}'
      ORDER BY max_updated_at DESC ;`
  );

  res.json(abc);
});

app.get("/getThread/:id", async (req, res) => {
  const id = parseInt(req.params.id);
  const thread = await client.query(
    `select * from threads left join users on threads.host_id = users.id where threads.id=${id}`
  );
  res.json(thread);
});
app.get("/allComment/:id", async (req, res) => {
  const id = parseInt(req.params.id);
  const comment = await client.query(
    `select * from comments left join users on comments.user_id = users.id where comments.thread_id=${id}`
  );
  res.json(comment);
});

app.post("/comment/:id", upload.single("image"), async (req, res) => {
  const userFound = req.session["user"];
  console.log(userFound.id);

  const comment = req.body;
  const id = parseInt(req.params.id);

  if (req.file) {
    comment.image = req.file.filename;
  }

  await client.query(
    /*sql*/ `INSERT INTO comments (user_id,content,image,created_at,updated_at,upvote,thread_id) VALUES ($1,$2,$3,NOW(),NOW(),$4,$5)`,
    [userFound.id, comment.content, comment.image, 0, id]
  );

  res.json({ success: true });
});

app.post("/newThread", upload.single("image"), async (req, res) => {
  const userFound = req.session["user"];
  console.log(userFound.id);

  const thread = req.body;

  if (req.file) {
    thread.image = req.file.filename;
  }

  const returnId = await client.query(
    /*sql*/ `INSERT INTO threads (host_id,route_id,title,content,image,upvote,created_at) VALUES ($1,$2,$3,$4,$5,$6,NOW()) returning id`,
    [
      userFound.id,
      thread.routeName,
      thread.title,
      thread.content,
      thread.image,
      0,
    ]
  );

  res.json(returnId);
});

app.get("/getChatHistory", async (req, res) => {
  const userFound = req.session["user"];
  const chatHistory = await client.query(
    `select * from guild_chat left join users on guild_chat.user_id = users.id where  guild_chat.guild_id = ${userFound.guild_id}`
  );
  const guildName = await client.query(
    `select name from guilds where id = ${userFound.guild_id} `
  );
  res.json({
    chatHistory: chatHistory,
    id: userFound.id,
    my_propic: userFound.profilepic,
    guildName: guildName,
  });
});

app.get("/getActivityChatHistory/:activityID", async (req, res) => {
  const activityID = parseInt(req.params.activityID);
  const userFound = req.session["user"];
  const chatHistory = await client.query(
    `select * from activity_chat left join users on activity_chat.user_id = users.id where activity_chat.activity_id = ${activityID}`
  );
  res.json({
    chatHistory: chatHistory,
    id: userFound.id,
    my_propic: userFound.profilepic,
  });
});

app.post("/sendChat", async (req, res) => {
  const userFound = req.session["user"];
  console.log(userFound);
  console.log(req.body);
  await client.query(
    `INSERT INTO guild_chat (guild_id,user_id,content,created_at) VALUES ($1,$2,$3,NOW())`,
    [userFound.guild_id, userFound.id, req.body.msg]
  );
  res.json({ success: true });
});

app.post("/sendActivityChat/:activityID", async (req, res) => {
  const activityID = parseInt(req.params.activityID);
  console.log(activityID + "!!!!");
  const userFound = req.session["user"];
  console.log(userFound);
  console.log(req.body);
  await client.query(
    `INSERT INTO activity_chat (activity_id,user_id,content,created_at) VALUES ($1,$2,$3,NOW())`,
    [activityID, userFound.id, req.body.msg]
  );
  res.json({ success: true });
});

app.use(express.static("public"));
app.use(isLoggedIn, express.static("protected"));
app.use("/index", express.static("/index/index.html"));

app.use((req, res) => {
  res.sendFile(path.resolve("./public/404.html"));
});
//set port number and listen to it
const PORT = 8080;

client.connect().then(() => {
  server.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
  });
});

function getDistanceFromLatLonInKm(
  lat1: number,
  lon1: number,
  lat2: number,
  lon2: number
) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2 - lat1); // deg2rad below
  var dLon = deg2rad(lon2 - lon1);
  var a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) *
    Math.cos(deg2rad(lat2)) *
    Math.sin(dLon / 2) *
    Math.sin(dLon / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c; // Distance in km
  return d;
}

function deg2rad(deg: number) {
  return deg * (Math.PI / 180);
}
